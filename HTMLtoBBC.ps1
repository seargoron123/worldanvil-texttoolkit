function Convert-HTMLSiteToBBCode {
    param(
        # URL for document to scrape
        [Parameter(Mandatory=$false)]
        [string]
        $WebURI,

        # URL for document to scrape
        [Parameter(Mandatory=$false)]
        [string]
        $filePath,

        # URL for document to scrape
        [Parameter(Mandatory=$false)]
        [string]
        $outputFolder,

        # Turn off to keep divs in the output
        [Parameter(Mandatory=$false)]
        [bool]
        $removeDivs = $true,

        # Turn off to keep navs in the output
        [Parameter(Mandatory=$false)]
        [bool]
        $removeNavs = $true,

        # Turn off to keep scripts in the output
        [Parameter(Mandatory=$false)]
        [bool]
        $removeScripts = $true
    )
    $isWebPage = $true
    [string[]]$content = @()
    if ($null -ne $WebURI -and "" -ne $WebURI)
    {
        $site = Invoke-WebRequest($WebURI)
        $content = $site.Content -split '\r?\n'
    }elseif ($null -ne $filePath -and "" -ne $filePath){
        $isWebPage = $false
        $content = Get-Content -Path $filePath
    }else{
        return;
    }
    $mainStart = $content | Where-Object {$_ -like "*<main*"} | Select-Object -First 1
    $mainEnd = $content | Where-Object {$_ -like "*</main*"} | Select-Object -First 1
    if (($null -eq $mainStart -or "" -eq $mainStart) -or ($null -eq $mainEnd -or "" -eq $mainEnd)){
        $mainStart = $content | Where-Object {$_ -like "*<body*"} | Select-Object -First 1
        $mainEnd = $content | Where-Object {$_ -like "*</body*"} | Select-Object -First 1
    }
    if (($null -ne $mainStart -and "" -ne $mainStart) -and ($null -ne $mainEnd -and "" -ne $mainEnd)){
        $main = $content | Where-Object {$null -ne $_ -and "" -ne $_ -and $_ -notlike "*<!--*" -and ($content.IndexOf($_) -gt $content.IndexOf($mainStart)) -and ($content.IndexOf($_) -lt $content.IndexOf($mainEnd))}
    }else{
        $main = $content
    }
    $main = $main.Trim()
    if ($main.Contains('<div id="page-content">')){
        $contentIndex = $main.IndexOf('<div id="page-content">')
        $main = $main | Where-Object {$main.IndexOf($_) -gt $contentIndex}
    }
    if ($removeDivs){
        $nodivs = $main | Where-Object {$_ -notlike "<*div*>*"}
        $main = $nodivs
    }

    if ($removeNavs){
        [string[]]$navs = $main | Where-Object {$_ -like "*<nav*"}
        [string[]]$nonavs = @()
        if($navs.length -gt 0){
            foreach($n in $navs){
                $endNav = $main | Where-Object {$_ -like "</nav*"} | Select-Object -First 1
                $startIndex = $main.IndexOf($n)
                $endIndex = $main.IndexOf($endNav)
                $nonavs = $main | Where-Object {($main.IndexOf($_) -lt $startIndex) -or ($main.IndexOf($_) -gt $endIndex)}
            }
            $main = $nonavs
        }
    }

    if ($removeScripts){
        [string[]]$noscripts = @()
        $noscripts = $main | Where-Object {$_ -notlike "<script>"}
        $noscripts = $noscripts | Where-Object {$_ -notlike "</script>"}
        [string[]]$scriptStarts = $noscripts | Where-Object {$_ -like "*{*"}
        [string[]]$scriptEnds = $noscripts | Where-Object {$_ -like "*}*"}
        if($scriptStarts.length -gt 0 -and $scriptEnds.length -gt 0){
            foreach($s in $scriptStarts){
                $scriptStartIndex = $noscripts.IndexOf($s)
                $scriptEnd = $scriptEnds[$scriptStarts.IndexOf($s)]
                $scriptEndIndex = $noscripts.IndexOf($scriptEnd)
                $noscripts = $main | Where-Object {(($main.IndexOf($_) -lt $scriptStartIndex)) -or (($main.IndexOf($_) -gt $scriptEndIndex))}
            }
        }
        $main = $noscripts
    }
    [string[]]$removeRegExs = @()
    $removeRegExs += '(?ms)(?<=^\s*<h1\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<h2\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<h3\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<h4\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<p\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<table\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<th\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<tr\s*).*?(?=\s*>)'
    $removeRegExs += '(?ms)(?<=^\s*<td\s*).*?(?=\s*>)'
    [string[]]$newmain=@()
    foreach($l in $main){
        foreach ($r in $removeRegExs){
            $l = $l -replace $r, ''
        }
        $l = $l -replace "<","["
        $l = $l -replace ">","]"
        $l = $l -replace "&apos;","'"
        $l = $l -replace "&rsquo;",'"'
        $l = $l -replace "&nbsp;"," "
        $l = $l -replace ([regex]::Escape("[span]")),""
        $l = $l -replace ([regex]::Escape("[/span]")),""
        $l = $l -replace ([regex]::Escape("[tbody]")),""
        $l = $l -replace ([regex]::Escape("[/tbody]")),""
        $l = $l -replace ([regex]::Escape("[a href=")),"[url:"
        $l = $l -replace ([regex]::Escape("[/a]")),"[/url]"
        $l = $l -replace ([regex]::Escape("[em]")),"[i]"
        $l = $l -replace ([regex]::Escape("[/em]")),"[/i]"
        $l = $l -replace ([regex]::Escape("[strong]")),"[b]"
        $l = $l -replace ([regex]::Escape("[/strong]")),"[/b]"
        
        $newmain += $l
    }
    $main = $newmain
    [string]$outputName = ""
    if ($isWebPage){
        $URIParts = $WebURI.Split("/")
        $outputName = $URIParts[$URIParts.length - 1].Replace(':','_')
    }else{
        $outputName = $filePath.Split("\")[-1]
    }
    if ($null -ne $outputFolder -and "" -ne $outputFolder){
        $main | Out-File "$outputFolder\$outputName.bbcode"
    }else{
        $main | Out-File ".\output\$outputName.bbcode"
    }
}
